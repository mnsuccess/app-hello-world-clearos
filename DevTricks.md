# Dev tricks

## Mounting remote folder on local machine

While using locally to mount a folder from a Virtual Machine on host, or even from a server on the same network it is possible to safely mount a remote folder on local machine.

My ClearOS server has a development folder on '/root/apps', but I would like to work over that folder using tools on my local machine, at '/home/fabio/Work/avantech/clearapps'. We can use NFS to mount remote folder locally.

If you and your server are not together in a safe network, this trick is extremely dangerous. Don't do it.

On ClearOS server:

1. Let's setup the shared folder
```
# cat >> /etc/exports << EOF
/root/apps    *(rw,sync,no_subtree_check,no_root_squash)
EOF
```

2. Reload the NFS exports
```
# exportfs -arv
```

On local host

3. Mount remote(192.168.56.101) folder
```
# mount -t nfs -o vers=4 192.168.56.101:/root/apps /home/fabio/Work/avantech/clearapps
```

4. Relax permission to edit
```
GROUP=$(awk -F: "/:$GID:/{ print \$1 }" /etc/group)
# chmod -R g+w /home/fabio/Work/avantech/clearapps
# chgrp -R $GROUP /home/fabio/Work/avantech/clearapps
```
